# Assignment 2 - Agile Software Practice.

# Missing Paws

Name: Mozeeb Abdulha

## Client UI.

...... Show screenshots of each view/page in your Vue app - include a short caption stating the purpose of each one .......

![][homepage]

>>Gives information to the user about what they can do on the site

![][petslist]

>>Displays all the missing pets in a table

![][managepets]

>>Owners of the pets can edit their pets details or remove the pet

![][reportpet]

>>Allows the owners to report a missing pet in order to get help to find it

![][edit]

>>Allows the owners only to edit information about their pets

![][login]

>>Allows users who have already registered login and add their pets

![][register]

>>Allows users to make an account to the app

## E2E/Cypress testing.

(Optional) State any non-standard features (not covered in the lectures or sample code provided) of the Cypress framework that you utilized.

## Web API CI.

(Optional) State the GitLab Pages URL of the coverage report for your Web API tests

https://gitlab.com/mabdulha/missing-paws-api-cicd/-/jobs/376119164/artifacts/browse/coverage/lcov-report/
## GitLab CI.

Made video recordings of the running tests in gitlab-ci.yml
Deployment to firebase and surge


[homepage]: ./img/homepage.png
[petslist]: ./img/petslist.png
[managepets]: ./img/managepets.png
[reportpet]: ./img/reportpet.png
[edit]: ./img/edit.png
[login]: ./img/login.png
[register]: ./img/register.png